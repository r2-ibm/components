# Changelog
All notable changes to this job will be documented in this file.

## [1.1.0] - 2022-06-13
* Add docker image tag in variable 

## [1.0.0] - 2022-04-14
* Change the default stage into `tests`

## [0.2.0] - 2022-04-06
* Change Docker image version of Gitleaks 

## [0.1.0] - 2020-11-10
* Initial version